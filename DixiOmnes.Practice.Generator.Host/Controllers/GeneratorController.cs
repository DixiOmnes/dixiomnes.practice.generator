﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace DixiOmnes.Practice.Generator.Host.Controllers
{
    public class GeneratorController : Controller
    {
        [HttpPost("/api/1.0/generate_names/{count:int}")]
        public IActionResult GenerateNames(int count)
        {
            var array = new int[count];
            var rnd = new Random();

            for (int i=0; i< count; i++)
            {
                array[i] = rnd.Next(1, 100);
            }


            return Ok();
        }
    }
}